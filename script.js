// ## Завдання
//
// - Створити сторінку, яка імітує стрічку новин соціальної мережі [Twitter](https://twitter.com/).
//
// #### Технічні вимоги:


//  - При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
//    Для цього потрібно надіслати GET запит на наступні дві адреси:
//    - `https://ajax.test-danit.com/api/json/users`
//    - `https://ajax.test-danit.com/api/json/posts`
//  - Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//  - Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст,
//    а також ім'я, прізвище та імейл користувача, який її розмістив.
//  - На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
//    При натисканні на неї необхідно надіслати DELETE запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`.
//    Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.

//  - Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти
//    [тут](https://ajax.test-danit.com/api-pages/jsonplaceholder.html).

//  - Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там
//    збережені. Це нормально, все так і має працювати.

//  - Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас `Card`.
//    При необхідності ви можете додавати також інші класи.
//
//  - Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження.
//    Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.

const root = document.querySelector('#root');
const BASE_URL = 'https://ajax.test-danit.com/api/json/';

const users = 'users';
const posts = 'posts';


class Request {

    getOne(slug, id) {
        return fetch(BASE_URL + slug + '/' + id)
            .then((response) => response.json())
            .catch((error) => console.log(error))
    }

    getAll(slug) {
        return fetch(BASE_URL + slug)
            .then((response) => response.json())
            .catch((error) => console.log(error))
    }

    removeOne(slug, id) {
        return fetch(`${BASE_URL}${slug}/${id}`, {
            method: 'DELETE',
        })
            .then((response) => response)
            .catch((error) => console.log(error))
    }
}

const request = new Request();

class Card {
    render(array) {

        const fragment = document.createDocumentFragment();
        const template = document.querySelector('#card').content;

        const [posts, users] = array;

        posts.forEach((el) => {
            const card = template.querySelector('.card').cloneNode(true);

            const postId = el.id;
            const postTitle = card.querySelector('.card-title');
            const postText = card.querySelector('.card-text');
            const person = card.querySelector('.user--name');
            const email = card.querySelector('.user-email');

            const btnDelete = card.querySelector('.card--button__delete')

            let user = users.find(item => item.id === el.userId)

            postTitle.textContent = el.title.charAt(0).toUpperCase() + el.title.slice(1);
            postText.textContent = el.body.charAt(0).toUpperCase() + el.body.slice(1);
            person.textContent = user.name.trim();
            email.textContent = user.email.trim().toLowerCase();

            btnDelete.addEventListener('click', () => {
                this.onClickRemoveCard(postId, card);
            });

            // Вивід карток у зворотньому порядку
            fragment.prepend(card);
        })

        return fragment;
    }

    onClickRemoveCard(id, card) {
        request.removeOne(posts, id)
            .then((response) => {
                if (response.status === 200) {
                    card.remove()
                }
            })
    }

    showLoader(){
        const loader = document.createElement('div');
        loader.classList.add('loader');

        return loader;
    }
}

const card = new Card();

const promisePosts = request.getAll(posts).then((data) => data);
const promiseUsers = request.getAll(users).then((data) => data);

root.append(card.showLoader());

Promise.all([promisePosts, promiseUsers]).then((data) => {
    root.removeChild(root.firstChild);
    root.append(card.render(data));
});



// #### Необов'язкове завдання підвищеної складності
//
//  - Додати зверху сторінки кнопку `Додати публікацію`. При натисканні на кнопку відкривати модальне вікно,
//    в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно
//    надіслати в POST запиті на адресу: `https://ajax.test-danit.com/api/json/posts`. Нова публікація має бути додана
//    зверху сторінки (сортування у зворотному хронологічному порядку).
//
//    Автором можна присвоїти публікації користувача з `id: 1`.

//  - Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін
//    необхідно надіслати PUT запит на адресу `https://ajax.test-danit.com/api/json/posts/${postId}`.

